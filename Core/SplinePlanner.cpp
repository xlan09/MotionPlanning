#include "SplinePlanner.h"
#include <stdexcept>
#include <sstream>
#include <cmath>
#include "Util.h"
#include "simulator.h"
#include "Predictor.h"
#include "Core/Estimator.h"
#include <algorithm>
#include <Core/SharedDataService.h>
#include <chrono>
#include <algorithm>
#include "spdlog/spdlog.h"

Point::Point() : m_x(0), m_y(0)
{

}

Point::Point(double x) : m_x(x), m_y(0)
{

}

Point::Point(double x, double y) : m_x(x), m_y(y)
{

}

double &Point::operator[](unsigned int i)
{
    if(i == 0) return m_x;
    else if(i == 1) return m_y;
    else throw std::invalid_argument("Index out of range when accessing point!");
}

const double &Point::operator [](unsigned int i) const
{
    if(i == 0) return m_x;
    else if(i == 1) return m_y;
    else throw std::invalid_argument("Index out of range when accessing point!");
}

Point &Point::operator +=(const Point &rhs)
{
    m_x += rhs.m_x;
    m_y += rhs.m_y;
    return *this;
}

Point &Point::operator +=(double rhs)
{
    m_x += rhs;
    m_y += rhs;

    return *this;
}

Point &Point::operator -=(const Point &rhs)
{
    m_x -= rhs.m_x;
    m_y -= rhs.m_y;

    return *this;
}

Point &Point::operator -=(double rhs)
{
    m_x -= rhs;
    m_y -= rhs;

    return *this;
}

Point &Point::operator *=(const Point &rhs)
{
    m_x *= rhs.m_x;
    m_y *= rhs.m_y;

    return *this;
}

Point &Point::operator *=(double rhs)
{
    m_x *= rhs;
    m_y *= rhs;

    return *this;
}

std::ostream &operator <<(std::ostream &os, const Point &point)
{
    std::stringstream ss;
    ss << point;
    os << ss.str();
    return os;
}

std::stringstream &operator <<(std::stringstream &ss, const Point &point)
{
    ss << "[" << point.m_x << ", " << point.m_y << "]";
    return ss;
}


Point operator +(const Point &lhs, const Point &rhs)
{
    Point sum = lhs;
    sum += rhs;
    return sum;
}


Point operator -(const Point &lhs, const Point &rhs)
{
    Point sum = lhs;
    sum -= rhs;
    return sum;
}


Point operator *(const Point &lhs, double rhs)
{
    Point product = lhs;
    product *= rhs;
    return product;
}


Point operator *(double lhs, const Point &rhs)
{
    Point product = rhs;
    product *= lhs;
    return product;
}

Spline::Spline(const std::vector<Point> &controlPoints, unsigned int degree, double timeSpan) : m_controlPoints(controlPoints), m_degree(degree)
{
    if (controlPoints.size() < degree + 1) throw std::invalid_argument("Number of control points can not be less than spline degree + 1");
    std::vector<double> internalKnots = Util::linspace(0, timeSpan, controlPoints.size() - degree + 1);
    // use clamped spline, so make the first d knots and last d knots equal
    // this way we will make sure the spline cross the last control point
    for(unsigned int i = 0; i< degree; ++i)
    {
        internalKnots.push_back(internalKnots.back());
        internalKnots.insert(internalKnots.begin(), internalKnots[0]);
    }

    m_knots = internalKnots;
}

Point Spline::value(double t) const
{
    Point res;
    for(std::size_t i = 0; i < m_controlPoints.size(); ++i)
    {
        res += b_i_d(i, m_degree, t) * m_controlPoints[i];
    }

    return res;
}
std::vector<double> Spline::knots() const
{
    return m_knots;
}

double Spline::b_i_0(unsigned int i, double t) const
{
    if (i > m_controlPoints.size() + m_degree - 1)
    {
        throw std::invalid_argument("knots vector index out of range");
    }

    // if two knots are equal, return 0
    if (Util::isDoubleEqual(m_knots[i], m_knots[i+1])) return 0;

    if (m_knots[i] <= t && t <= m_knots[i+1]) return 1;
    else return 0;
}

double Spline::b_i_d(unsigned int i, unsigned int degree, double t) const
{
    if(degree == 0)
    {
        return b_i_0(i, t);
    }

    // if two time knots are equal
    if (Util::isDoubleEqual(m_knots[i + degree], m_knots[i]) && Util::isDoubleEqual(m_knots[i + degree + 1], m_knots[i + 1]))
    {
        return 0;
    }
    else if (Util::isDoubleEqual(m_knots[i + degree], m_knots[i]))
    {
        return (m_knots[i + 1 + degree] - t) / (m_knots[i + degree + 1] - m_knots[i + 1]) * b_i_d(i+1, degree - 1, t);
    }
    else if (Util::isDoubleEqual(m_knots[i + degree + 1], m_knots[i + 1]))
    {
        return (t - m_knots[i]) / (m_knots[i + degree] - m_knots[i]) * b_i_d(i, degree - 1, t);
    }
    else
    {
        return (t - m_knots[i]) / (m_knots[i + degree] - m_knots[i]) * b_i_d(i, degree - 1, t) + \
               (m_knots[i + 1 + degree] - t) / (m_knots[i + degree + 1] - m_knots[i + 1]) * b_i_d(i+1, degree - 1, t);
    }
}

SplinePlanner::SplinePlanner(double maxAngularVelocity, double neighborhoodRadius, double dt, std::shared_ptr<SharedDataService> sharedDataService, const points_2d &walls, unsigned int recedingHorizon) : m_maxAngularVelocity(maxAngularVelocity), m_neighorhoodRadius(neighborhoodRadius), m_dt(dt), m_sharedDataService(sharedDataService), m_walls(walls), m_recedingHorizon(recedingHorizon)
{
    std::string loggerName = "Planner", logFilename = "LogPlanner";
    if (spdlog::get(loggerName) != nullptr)
    {
        m_logger = spdlog::get(loggerName);
    }
    else
    {
        m_logger = spdlog::rotating_logger_mt(loggerName, logFilename, 1024 * 1024 * 10, 5);
    }
}

void SplinePlanner::run()
{
    unsigned int nextAllowedComputationTimeSteps = 0, computationTimeStepsBuffer = 0;
    while(true)
    {
        try
        {
            m_logger->info("*********************start planning************************");
            std::chrono::steady_clock::time_point startTime = std::chrono::steady_clock::now();

            std::vector<point_2d> plannedCommands = plan();

            int calculationTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - startTime).count(); // unit is milli seconds
            nextAllowedComputationTimeSteps = std::max(nextAllowedComputationTimeSteps, (unsigned int)std::ceil(calculationTime / (m_dt * 1000)));
            m_logger->info("Planning takes {} milliseconds, control loop time step: {}, nextAllowedComputationSteps: {}", calculationTime, m_dt, nextAllowedComputationTimeSteps + computationTimeStepsBuffer);

            m_logger->info("Planned commands size is: {}", plannedCommands.size());
            for(unsigned int i = 0; i < plannedCommands.size(); ++i)
            {
                m_logger->info("vel: {}, steer: {}", plannedCommands[i].first, plannedCommands[i].second);
            }

            unsigned int currQueueSize = m_sharedDataService->queueSize();

            if(currQueueSize >= nextAllowedComputationTimeSteps + computationTimeStepsBuffer)
            {
                m_logger->info("Enough actions in the queue, will not push new actions!");
            }
            else
            {
                unsigned int numOfActionsNeedToPushToQueue = nextAllowedComputationTimeSteps + computationTimeStepsBuffer - currQueueSize;
                if (plannedCommands.size() < numOfActionsNeedToPushToQueue)
                {
                    // In this case, it's too late to avoid obstacles, since we use receding horizon and predict forward, we should avoid this situtation. Or we can apply max steer in this situation.
                    m_logger->warn("Not enough actions planned, environment may be too cluttered");
                }

                unsigned int numOfActionsWillBePushedToQueue = std::min(numOfActionsNeedToPushToQueue, (unsigned int)plannedCommands.size());
                m_logger->info("CurrQueueSize: {}, first {} of planned commands will be pushed to queue", currQueueSize, numOfActionsWillBePushedToQueue);
                for(unsigned int i = 0; i < numOfActionsWillBePushedToQueue; ++i)
                {
                    m_sharedDataService->pushToQueue(plannedCommands[i]);
                }
            }
        }
        catch(const std::exception &exp)
        {
            m_logger->error("Exception thrown: {}", exp.what());
        }
    }
}

std::vector<point_2d> SplinePlanner::plan() const
{
    sim::simulation_state curr = m_sharedDataService->getCurr();
    sim::simulation_state prev = m_sharedDataService->getPrev();
    sim::simulation_state prevOfPrev = m_sharedDataService->getPrevOfPrev();
    std::queue<point_2d> currQueue = m_sharedDataService->getCurrQueue();
    std::size_t lookAheadSteps = currQueue.size();

    m_logger->info("prev runtime: {}, vehicle pos, x: {}, y: {}, theta: {}", prev.run_time, prev.player_state.location.first, prev.player_state.location.second, prev.player_state.theta);
    m_logger->info("curr runtime: {}, vehicle pos, x: {}, y: {}, theta: {}, currQueueSize: {}", curr.run_time, curr.player_state.location.first, curr.player_state.location.second, curr.player_state.theta, lookAheadSteps);

    double dt = curr.run_time - prev.run_time;
    if (Util::isDoubleEqual(dt, 0))
    {
        m_logger->warn("prev and curr runtime are the same!");
        return std::vector<point_2d>();
    }

    // predict vehicle pose
    sim::state_SE2 vehiclePredictedPose = curr.player_state;
    while(!currQueue.empty())
    {
        point_2d front = currQueue.front();
        vehiclePredictedPose = Predictor::vehicleDynamics(vehiclePredictedPose, front.first, front.second, dt);
        currQueue.pop();
    }

    // predict obstacles pose
    std::unordered_map<guid, sim::state_SE2> predictedObstaclePoseMap;
    std::unordered_map<guid, point_2d> obsIdToCommandsMap;
    for(auto iter = curr.object_states.begin(); iter != curr.object_states.end(); ++iter)
    {
        // Estimate obstacle linear velocity and angular velocity
        double obsPrevHeading = vecHeading(prev.object_states.at(iter->first) - prevOfPrev.object_states.at(iter->first));
        double obsCurrHeading = vecHeading(curr.object_states.at(iter->first) - prev.object_states.at(iter->first));
        sim::state_SE2 obsCurrPose = sim::state_SE2(curr.object_states.at(iter->first), obsCurrHeading);
        sim::state_SE2 obsPrevPose = sim::state_SE2(prev.object_states.at(iter->first), obsPrevHeading);
        point_2d obsCommands = Estimator::estimate(obsCurrPose, obsPrevPose, dt);
        obsIdToCommandsMap[iter->first] = obsCommands;

        // predict obs position
        sim::state_SE2 nextObsPose = obsCurrPose;
        for(std::size_t i = 0; i < lookAheadSteps; ++i)
        {
            nextObsPose = Predictor::vehicleDynamics(nextObsPose, obsCommands.first, obsCommands.second, dt);
        }

        predictedObstaclePoseMap[iter->first] = nextObsPose;
    }

    // Start planning from the predicted statuses
    std::vector<guid> nearObsIds = findObstaclesInNeighborhood(predictedObstaclePoseMap, vehiclePredictedPose.location, m_neighorhoodRadius);
    // Fix linear velocity and we only control the angular velocity
    double velocityProfile = Estimator::estimate(curr.player_state, prev.player_state, dt).first;

    double maxCurvature = std::fabs(m_maxAngularVelocity / velocityProfile);
    unsigned int curvatureControlPointsSetSize = 5;
    std::vector<double> controlPointsSet = Util::linspace(-maxCurvature, maxCurvature, curvatureControlPointsSetSize);
    std::vector<std::vector<double> > candidateControlPoints;
    unsigned int splineDegree = 2;
    Util::iterProduct(controlPointsSet, splineDegree + 1, candidateControlPoints);

    // safe distance, obstacle and vehicle radius is 2.5
    double vehicleRadius = 2.5;
    double safeDistToObs = 2 * vehicleRadius + 1.5, safeDistToWall = (1 / maxCurvature) * 2 + vehicleRadius + 1;

    // logging
    m_logger->info("PredictionSteps: {}, predicted vehicle pose, x: {}, y: {}, theta: {}, num of obs in neighborhood: {}", lookAheadSteps, vehiclePredictedPose.location.first, vehiclePredictedPose.location.second, vehiclePredictedPose.theta, nearObsIds.size());
    m_logger->info("Spline degree: {}, maxcurvature: {}, search space size: {}, receding horizon: {}", splineDegree, maxCurvature, std::pow(curvatureControlPointsSetSize, splineDegree + 1), m_recedingHorizon);
    m_logger->info("SafeDistToObs: {}, safeDistToWall: {}", safeDistToObs, safeDistToWall);

    double maxTrajDistToObs = 0;
    std::vector<point_2d> plannedCommands;
    for(std::size_t i = 0; i < candidateControlPoints.size(); ++i)
    {
        std::vector<point_2d> trajectory;

        // steerAngles size is m_recedingHorizon + 1, trajectory size is m_recedingHorizon + 2
        std::vector<double> steerAngles = splineTrajectory(vehiclePredictedPose, velocityProfile, Util::generateControlPoints(candidateControlPoints[i]), splineDegree, m_recedingHorizon, dt, trajectory);

        unsigned int feasibleSteerEndPointIndex = steerAngles.size();
        double trajectoryDistToObs = evaluateTrajectory(nearObsIds, predictedObstaclePoseMap, obsIdToCommandsMap, m_walls, trajectory, dt, safeDistToObs, safeDistToWall, feasibleSteerEndPointIndex);

        if(trajectoryDistToObs > maxTrajDistToObs)
        {
            maxTrajDistToObs = trajectoryDistToObs;
            std::vector<point_2d> currTrajCommands;
            for(std::size_t k = 0; k < feasibleSteerEndPointIndex; ++k)
            {
                currTrajCommands.push_back(std::make_pair(velocityProfile, steerAngles[k]));
            }

            plannedCommands = currTrajCommands;
        }
    }

    return plannedCommands;
}

std::vector<double> SplinePlanner::splineTrajectory(const sim::state_SE2 &startPose, double linearVelocity, const std::vector<Point> &controlPoints, unsigned int splineDegree, unsigned int timeSteps, double dt, std::vector<point_2d> &trajectory) const
{
    Spline spline(controlPoints, splineDegree, dt * timeSteps);
    std::vector<double> times = Util::linspace(0, dt * timeSteps, timeSteps + 1);
    std::vector<double> steerAngles;
    for(std::size_t i = 0; i < times.size(); ++i)
    {
        // Point is curvature point, so it's a scalar
        steerAngles.push_back(linearVelocity * spline.value(times[i])[0]);
    }

    trajectory.clear();
    trajectory.push_back(startPose.location);

    sim::state_SE2 nextPose(startPose);
    for(std::size_t i = 0; i < steerAngles.size(); ++i)
    {
        nextPose = Predictor::vehicleDynamics(nextPose, linearVelocity, steerAngles[i], dt);
        trajectory.push_back(nextPose.location);
    }

    return steerAngles;
}

std::vector<guid> SplinePlanner::findObstaclesInNeighborhood(std::unordered_map<guid, sim::state_SE2> &predictedObstaclePoseMap, const point_2d &vehiclePos, double neighborhoodRadius) const
{
    std::vector<guid> res;
    for(std::unordered_map<guid, sim::state_SE2>::const_iterator iter = predictedObstaclePoseMap.begin(); iter != predictedObstaclePoseMap.end(); ++iter)
    {
        if (dist(iter->second.location, vehiclePos) <= neighborhoodRadius)
        {
            res.push_back(iter->first);
        }
    }

    return res;
}

double SplinePlanner::evaluateTrajectoryToObstacle(const sim::state_SE2 &obsPose, const point_2d &obsCommands, const points_2d &walls, const std::vector<point_2d> &trajectory, double dt, double safeDistToObs, double safeDistToWall, unsigned int &feasibleTrajEndPointIndex) const
{
    sim::state_SE2 obsNextPose = obsPose;
    std::vector<point_2d> obsTraj;
    obsTraj.push_back(obsNextPose.location);

    point_2d vehicleNextPosition = trajectory[0];
    feasibleTrajEndPointIndex = 0;

    int cushionSteps = 4;
    bool isCollision = false;
    for(int i = 0; i < ((int)trajectory.size()) - 1; ++i)
    {
        // vehicle and obstacle radius are 2.5
        if (dist(obsNextPose.location, vehicleNextPosition) < 5.5 || Util::isCollisionToWall(vehicleNextPosition, walls))
        {
            isCollision = true;
            // if collision, do not get too close to obtacles
            feasibleTrajEndPointIndex = (unsigned int)std::max(0, (int)feasibleTrajEndPointIndex - cushionSteps);
            break;
        }
        else
        {
            feasibleTrajEndPointIndex = i + 1;
            vehicleNextPosition = trajectory[i + 1];

            obsNextPose = Predictor::vehicleDynamics(obsNextPose, obsCommands.first, obsCommands.second, dt);
            obsTraj.push_back(obsNextPose.location);
        }
    }

    // if not collision, we do not want the vehicle get too close to walls
    if(!isCollision && (dist(trajectory.back(), obsTraj.back()) < safeDistToObs || Util::isCollisionToWall(trajectory.back(), walls, safeDistToWall)))
    {
        feasibleTrajEndPointIndex = (unsigned int)std::max(0, (int)feasibleTrajEndPointIndex - cushionSteps);
    }

    // cost
    double weightOnTravelTime = 0, weightOnDistToObs = 1;
    double endPointDistToObs = std::min(dist(trajectory[feasibleTrajEndPointIndex], obsTraj[feasibleTrajEndPointIndex]), \
                                        Util::distToWall(trajectory[feasibleTrajEndPointIndex], walls));

    return weightOnTravelTime * feasibleTrajEndPointIndex + weightOnDistToObs * endPointDistToObs;
}

double SplinePlanner::evaluateTrajectory(const std::vector<guid> &nearIds, const std::unordered_map<guid, sim::state_SE2> &predictedObstaclePoseMap, const std::unordered_map<guid, point_2d> &obsIdToCommandsMap, const points_2d &walls, const std::vector<point_2d> &trajectory, double dt, double safeDistToObs, double safeDistToWall, unsigned int &feasibleTrajEndPointIndex) const
{
    double minDistToObs = std::numeric_limits<double>::max();
    feasibleTrajEndPointIndex = trajectory.size() - 2;
    for(size_t i = 0; i < nearIds.size(); ++i)
    {
        guid obsId = nearIds[i];
        unsigned int feasibleTrajEndPointIndexForCurrObs = 0;
        double distToObs = evaluateTrajectoryToObstacle(predictedObstaclePoseMap.at(obsId), obsIdToCommandsMap.at(obsId), walls, trajectory, dt, safeDistToObs, safeDistToWall, feasibleTrajEndPointIndexForCurrObs);
        if(distToObs < minDistToObs)
        {
            minDistToObs = distToObs;
            feasibleTrajEndPointIndex = feasibleTrajEndPointIndexForCurrObs;
        }
    }

    return minDistToObs;
}
