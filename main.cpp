// Motion planner designed by Xiaodong Lan
//
// Created by Xiaodong Lan (xlan@bu.edu) on 2015-05-18
// Copyright (c) 2015 Xiaodong Lan. All rights reserved.

#define CATCH_CONFIG_RUNNER
#include "Test/catch.hpp"
#include <iostream>
#include <cmath>
#include <unistd.h>
#include "lib/simulator.h"
#include <memory>
#include "Core/Util.h"
#include <stdexcept>
#include <chrono>
#include "Core/SharedDataService.h"
#include "Core/SplinePlanner.h"
#include "Core/ScopedThread.h"
#include "spdlog/spdlog.h"

const double PI = 3.1415926;

int main(int argc, char* const argv[])
{
    // unit test, should be in its own project
    // TODO: remove this when done
    int unitTestRunResult = Catch::Session().run(argc, argv);
    if (unitTestRunResult != 0) throw std::runtime_error("Unit test failed!");

    // logger
    auto logger = spdlog::rotating_logger_mt("Main", "LogMain", 1024 * 1024 * 10, 5);
    sim::simulator simulator;

    /**
     * @brief bbox
     * A points2d in the form {bottom_left, top_left, bottom_right, top_right}
     * Coordinate system is like this:
     * /\
     * |
     * |
     * |(bottome_left)
     * |------------------------------>
     * vehicle and obstacle radius is 2.5 unit
     */
    sim::points_2d walls = simulator.get_bounding_box();
    sim::simulation_state prevOfPrev = simulator.initialize(15);
    sim::simulation_state prev = prevOfPrev;
    sim::simulation_state curr = prevOfPrev;

    // control loop rate, unit is seconds
    double dt = 0.025;

    double maxAngularVelocity = PI / 6, neighborhood = std::fabs(walls[2].first - walls[0].first) / 3.0;
    unsigned int recedingHorizon = 10;
    std::shared_ptr<SharedDataService> sharedDataPtr = std::shared_ptr<SharedDataService>(new SharedDataService());
    SplinePlanner planner(maxAngularVelocity, neighborhood, dt, sharedDataPtr, walls, recedingHorizon);

    // start planner thread
    std::thread th(&SplinePlanner::run, &planner);
    ScopedThread scopedTh(std::move(th));

    try
    {
        double control;
        while(1)
        {
            std::chrono::steady_clock::time_point startTime = std::chrono::steady_clock::now();
            std::chrono::steady_clock::time_point planningEndTime = startTime + std::chrono::milliseconds(((int)(dt * 1000)));
            if(!sharedDataPtr->isQueueEmpty())
            {
                control = sharedDataPtr->frontOfQueue().second;
                logger->info("Applied control: vel: {}, steer: {}", sharedDataPtr->frontOfQueue().first, control);
                sharedDataPtr->popFromQueue();
            }
            else
            {
                control = 0;
                logger->warn("Action queue is empty!");
            }

            // update previous state
            prevOfPrev = prev;
            prev = curr;

            curr = simulator.step(control);

            sharedDataPtr->setCurr(curr);
            sharedDataPtr->setPrev(prev);
            sharedDataPtr->setPrevOfPrev(prevOfPrev);

            std::this_thread::sleep_until(planningEndTime);
            logger->info("{} milliseconds elapsed, start new control loop cycle", std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - startTime).count());
        }
    }
    catch(const sim::simulator::simulation_halt_exception& e)
    {
        std::cout<<e.what()<<"\n"<<e.final_state;
    }

    return 0;
}
