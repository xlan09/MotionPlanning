TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += C++11

SOURCES += main.cpp \
    Core/Predictor.cpp \
    Core/Estimator.cpp \
    Core/Util.cpp \
    Core/SplinePlanner.cpp \
    Test/PlannerTest.cpp \
    Core/SharedDataService.cpp \
    Core/SharedDataServiceDataStore.cpp \
    Core/ScopedThread.cpp


include(deployment.pri)
qtcAddDeployment()

LIBS += -L$$PWD/lib/ -lsimulator -lGL -lGLU -lglut -lQtGui -lQtCore -lQtOpenGL -lpthread
INCLUDEPATH += $$PWD/lib/

HEADERS += \
    Core/Predictor.h \
    Core/Estimator.h \
    Core/Util.h \
    Core/SplinePlanner.h \
    lib/simulator.h \
    Test/catch.hpp \
    Core/SharedDataService.h \
    Core/SharedDataServiceDataStore.h \
    Core/ScopedThread.h
