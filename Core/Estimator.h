#ifndef ESTIMATOR_H
#define ESTIMATOR_H

#include <utility>
#include "Core/Util.h"

namespace sim {
struct state_SE2;
}

typedef std::pair<double, double> point_2d;

class Estimator
{
public:

    /**
     * @brief estimate
     * Estimate Dubin's vehicle linear velocity and angular velocity
     * @return A pair consists of {linear_velocity magnitude, signed angular velocity}
     */
    static point_2d estimate(const sim::state_SE2 &currPose, const sim::state_SE2 &prevPose, double time);

    /**
     * @brief estimate
     * Estimate the linear velocity of the vehicle if it moves along a straight line
     * @return
     */
    static point_2d estimate(const point_2d &currPosition, const point_2d &prevPosition, double time);
};

#endif // ESTIMATOR_H
