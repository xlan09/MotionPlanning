#include "Estimator.h"
#include <cmath>
#include <stdexcept>
#include "simulator.h"

const double PI = 3.1415926535897;

point_2d Estimator::estimate(const sim::state_SE2 &currPose, const sim::state_SE2 &prevPose, double time)
{
    double tolerance = std::pow(10, -10);
    double headingDiff = currPose.theta - prevPose.theta;

    if (std::fabs(headingDiff) > PI)
    {
        headingDiff = (-1) * (headingDiff / std::fabs(headingDiff)) * (2 * PI - std::fabs(headingDiff));
    }

    double angular_velocity = headingDiff / time;
    double linear_velocity = 0;

    if (std::fabs(angular_velocity) < tolerance)
    {
        linear_velocity = dist(currPose.location, prevPose.location) / time;
    }
    else
    {
        linear_velocity = ((currPose.location.first - prevPose.location.first) / (std::sin(currPose.theta) - std::sin(prevPose.theta))) * angular_velocity;
    }

    return std::make_pair(linear_velocity, angular_velocity);
}

point_2d Estimator::estimate(const point_2d &currPosition, const point_2d &prevPosition, double time)
{
    return (currPosition - prevPosition) / time;
}

