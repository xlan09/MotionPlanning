# Goal
Design controller to drive the vehicle as long as possible without hitting anything. 

# Constraints
- The vehicle's speed is not allowed to adjust
- You can change your steering angle command (within a min and max range [-PI/6, PI/6]) at any time
- Time always moves forward (even while you are processing)
- You can sense the world at the same rate that you change your steering command
- You have perfect information about the vehicle's position, heading and the moving obstacles' positions. Other informations are unknown. 
- The world is bounded to be within an axis-aligned box with which you can collide
- All objects (including yourself) are circles with a diameter (and effective wheelbase) of 5 units
- You start the simulation in the middle of the world; the movers start in random locations but will always be at least 1/10 the size of the world away from you

# Note:
- Simulation time starts once you initialize the library. 
- When you call step to advance the simulation state you will provide a steering command that controls your vehicle. This command is for the future simulations, that is, the vehicle will execute this command at next step.
- During each simulation step the simulator will throw an exception if your robot hits anything – including the moving obstacles or the walls of the world.

# Controller
- Spline-based planner running on its own thread, communicates with the controller thread with a message queue.

- At each planning cycle, forward predict vehicle and obstacle positions for a dynamic horizon, planning will start from the end of the look-ahead horizon. This will account for the time consumed for planning.

- When planning, search for all the obstacles inside a neighborhood of the vehicle, only consider the obstacles inside the neighborhood, use receding horizon to look ahead,  search for all the candidate spline trajectories parametrized by 3 control points, choose the one which is collision free and farthest away from walls and obstacles.

- ![Gui does not show properly, click the png files to view](SplineBasedPlanner.png)

# Dependencies
- LIBS += -L$$PWD/lib/ -lsimulator -lGL -lGLU -lglut -lQtGui -lQtCore -lQtOpenGL -lpthread
- Unit test framework: catch
- Logging framework: spdlog

# GUI

![Image missing, go to files and check Gui.png](Gui.png)
