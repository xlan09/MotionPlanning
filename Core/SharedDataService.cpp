#include "SharedDataService.h"

SharedDataService::SharedDataService()
{
    m_datastore = std::shared_ptr<SharedDataServiceDataStore<point_2d>>(new SharedDataServiceDataStore<point_2d>());
}

void SharedDataService::pushToQueue(const point_2d &val)
{
    m_datastore->pushToQueue(val);
}

void SharedDataService::popFromQueue()
{
    m_datastore->popFromQueue();
}

point_2d SharedDataService::frontOfQueue() const
{
    return m_datastore->frontOfQueue();
}

bool SharedDataService::isQueueEmpty() const
{
    return m_datastore->isQueueEmpty();
}

std::size_t SharedDataService::queueSize() const
{
    return m_datastore->queueSize();
}

std::queue<point_2d> SharedDataService::getCurrQueue() const
{
    return m_datastore->getCurrQueue();
}

sim::simulation_state SharedDataService::getCurr() const
{
    return m_datastore->getCurr();
}

void SharedDataService::setCurr(const sim::simulation_state &curr)
{
    m_datastore->setCurr(curr);
}

sim::simulation_state SharedDataService::getPrev() const
{
    return m_datastore->getPrev();
}

void SharedDataService::setPrev(const sim::simulation_state &prev)
{
    m_datastore->setPrev(prev);
}

sim::simulation_state SharedDataService::getPrevOfPrev() const
{
    return m_datastore->getPrevOfPrev();
}

void SharedDataService::setPrevOfPrev(const sim::simulation_state &prevOfPrev)
{
    m_datastore->setPrevOfPrev(prevOfPrev);
}
