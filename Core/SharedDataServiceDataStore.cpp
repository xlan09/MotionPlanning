#include "SharedDataServiceDataStore.h"

template<typename T>
SharedDataServiceDataStore<T>::SharedDataServiceDataStore()
{
}

template<typename T>
void SharedDataServiceDataStore<T>::pushToQueue(const T &val)
{
    std::lock_guard<std::mutex> lock(m_mu);
    m_actionQueue.push(val);
}

template<typename T>
void SharedDataServiceDataStore<T>::popFromQueue()
{
    std::lock_guard<std::mutex> lock(m_mu);
    if(!m_actionQueue.empty())
    {
        m_actionQueue.pop();
    }
    else
    {
        throw std::logic_error("Calling pop of an empty queue is undefined behaviour!");
    }
}

template<typename T>
T SharedDataServiceDataStore<T>::frontOfQueue() const
{
    std::lock_guard<std::mutex> lock(m_mu);
    if(!m_actionQueue.empty())
    {
        return m_actionQueue.front();
    }
    else
    {
        throw std::logic_error("Calling front of an empty queue is undefined behaviour!");
    }
}

template<typename T>
bool SharedDataServiceDataStore<T>::isQueueEmpty() const
{
    std::lock_guard<std::mutex> lock(m_mu);
    return m_actionQueue.empty();
}

template<typename T>
std::size_t SharedDataServiceDataStore<T>::queueSize() const
{
    std::lock_guard<std::mutex> lock(m_mu);
    return m_actionQueue.size();
}

template<typename T>
std::queue<T> SharedDataServiceDataStore<T>::getCurrQueue() const
{
    std::lock_guard<std::mutex> lock(m_mu);
    return m_actionQueue; // create a copy and return
}

template<typename T>
sim::simulation_state SharedDataServiceDataStore<T>::getCurr() const
{
    std::lock_guard<std::mutex> lock(m_mu);
    return m_curr;
}

template<typename T>
void SharedDataServiceDataStore<T>::setCurr(const sim::simulation_state &curr)
{
    std::lock_guard<std::mutex> lock(m_mu);
    m_curr = curr;
}

template<typename T>
sim::simulation_state SharedDataServiceDataStore<T>::getPrev() const
{
    std::lock_guard<std::mutex> lock(m_mu);
    return m_prev;
}

template<typename T>
void SharedDataServiceDataStore<T>::setPrev(const sim::simulation_state &prev)
{
    std::lock_guard<std::mutex> lock(m_mu);
    m_prev = prev;
}

template<typename T>
sim::simulation_state SharedDataServiceDataStore<T>::getPrevOfPrev() const
{
    std::lock_guard<std::mutex> lock(m_mu);
    return m_prevOfPrev;
}

template<typename T>
void SharedDataServiceDataStore<T>::setPrevOfPrev(const sim::simulation_state &prevOfPrev)
{
    std::lock_guard<std::mutex> lock(m_mu);
    m_prevOfPrev = prevOfPrev;
}

template class SharedDataServiceDataStore<std::pair<double, double> >;
