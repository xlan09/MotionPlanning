#include "Util.h"
#include <cmath>
#include <stdexcept>
#include <lib/simulator.h>
#include <algorithm>
#include "Core/SplinePlanner.h"

const double PI = 3.1415926535;

bool Util::isCollisionToWall(const point_2d &currPos, const points_2d &walls, double cushion)
{
    bool isCollision  = false;
    if (currPos.first <= walls[0].first + cushion || currPos.first >= walls[2].first - cushion || currPos.second <= walls[0].second + cushion || currPos.second >= walls[1].second - cushion)
    {
        isCollision = true;
    }

    return isCollision;
}

double Util::distToWall(const point_2d &currPos, const points_2d &walls)
{
    double minDistToEastAndWestWalls = std::min(std::fabs(currPos.first - walls[0].first), std::fabs(currPos.first - walls[2].first));
    double minDistToNorthAndSouthWalls = std::min(std::fabs(currPos.second - walls[0].second), std::fabs(currPos.second - walls[1].second));

    return std::min(minDistToEastAndWestWalls, minDistToNorthAndSouthWalls);
}

bool Util::isDoubleEqual(double lhs, double rhs)
{
    double tolerance = std::pow(10, -10);
    return std::fabs(lhs - rhs) < tolerance;
}

std::vector<double> Util::linspace(double start, double end, unsigned int step)
{
    if (end <= start) throw std::invalid_argument("end must be greater than start");
    if (step <= 1) throw std::invalid_argument("step must be greater than 1");
    double interval = (end - start) / (step - 1);

    double element = start;
    std::vector<double> res;
    while(element < end)
    {
        res.push_back(element);
        element += interval;
    }

    res.push_back(end);

    return res;
}

void Util::iterProduct(const std::vector<double> &valueSet, unsigned int n, std::vector<std::vector<double> > &out)
{
    out.clear();
    for(unsigned int i = 0; i < valueSet.size(); ++i)
    {
        std::vector<std::vector<double> > allPathsInOneTree;
        std::vector<double> path;
        DFS(valueSet, valueSet[i], allPathsInOneTree, path, n - 1);

        for(unsigned int j = 0; j < allPathsInOneTree.size(); ++j)
        {
            out.push_back(allPathsInOneTree[j]);
        }
    }
}

std::vector<Point> Util::generateControlPoints(const std::vector<double> &values)
{
    std::vector<Point> res;
    for(std::size_t i = 0; i < values.size(); ++i)
    {
        res.push_back(Point(values[i]));
    }

    return res;
}

void Util::DFS(const std::vector<double> &set, double currNode, std::vector<std::vector<double> > &res, std::vector<double> &currPath, unsigned int n)
{
    currPath.push_back(currNode);
    if(n == 0)
    {
        res.push_back(currPath);
        return;
    }

    // look at all the children
    for(std::size_t i = 0; i < set.size(); ++i)
    {
        DFS(set, set[i], res, currPath, n - 1);
        currPath.pop_back();
    }
}

point_2d &operator*=(point_2d &arg, double num)
{
    arg.first *= num;
    arg.second *= num;
    return arg;
}

point_2d operator*(const point_2d &arg, double num)
{
    point_2d res = arg;
    res *= num;
    return res;
}


point_2d operator*(double num, const point_2d &arg)
{
    return arg * num;
}


point_2d &operator/=(point_2d &arg, double num)
{
    arg *= (1 / num);
    return arg;
}


point_2d operator/(const point_2d &arg, double num)
{
    return arg * (1 / num);
}


point_2d operator/(double num, const point_2d &arg)
{
    return arg / num;
}


point_2d &operator +=(point_2d &arg1, const point_2d &arg2)
{
    arg1.first += arg2.first;
    arg1.second += arg2.second;
    return arg1;
}


point_2d operator +(const point_2d &arg1, const point_2d &arg2)
{
    point_2d res = arg1;
    res += arg2;
    return res;
}


point_2d &operator -=(point_2d &arg1, const point_2d &arg2)
{
    arg1.first -= arg2.first;
    arg1.second -= arg2.second;
    return arg1;
}


point_2d operator -(const point_2d &arg1, const point_2d &arg2)
{
    point_2d res = arg1;
    res -= arg2;
    return res;
}


point_2d normalizeVec(const point_2d &arg)
{
    return arg * (1 / std::sqrt(arg.first * arg.first + arg.second * arg.second));
}


double dotProduct(const point_2d &vec1, const point_2d &vec2)
{
    return vec1.first * vec2.first + vec1.second * vec2.second;
}


double dist(const point_2d &vec1, const point_2d &vec2)
{
    return norm(vec1 - vec2);
}


double norm(const point_2d &vec)
{
    return std::sqrt(vec.first * vec.first + vec.second * vec.second);
}

std::string toString(const point_2d &vec)
{
    return "(x:" + std::to_string(vec.first) + ", y:" + std::to_string(vec.second) + ")";
}

std::string toString(const sim::simulation_state &state)
{
    std::ostringstream os;
    os << state;
    return os.str();
}

double vecHeading(const point_2d &arg)
{
    return std::atan2(arg.second, arg.first);
}
