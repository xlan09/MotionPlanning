#ifndef UTIL_H
#define UTIL_H
#include <utility>
#include <vector>
#include <string>
#include <unordered_map>
#include <sstream>
#include <map>

namespace sim
{
struct simulation_state;
struct state_SE2;
}

typedef std::pair<double, double> point_2d;

typedef std::vector<point_2d> points_2d;

typedef int64_t guid;

class Point;

class Util
{
public:

    /**
     * @brief isCloseToWalls
     * Check whether a point is close to wall, if it is, return true
     */
    static bool isCollisionToWall(const point_2d &currPos, const points_2d &walls, double cushion=0);

    static double distToWall(const point_2d &currPos, const points_2d &walls);

    static bool isDoubleEqual(double lhs, double rhs);
    static std::vector<double> linspace(double start, double end, unsigned int step);

    /**
     * @brief iterProduct, like python itertools.product
     * Choose n elements from a set, the order matters and if one element is chosen, we assume it still exist in the set.
     * calculate all the possible results.
     */
    static void iterProduct(const std::vector<double> &valueSet, unsigned int n, std::vector<std::vector<double> > &out);

    /**
     * @brief generateControlPoints
     */
    static std::vector<Point> generateControlPoints(const std::vector<double> &values);

private:
    static void DFS(const std::vector<double> &set, double currNode, std::vector<std::vector<double> > &res, std::vector<double> &currPath, unsigned int n);
};

point_2d &operator +=(point_2d &arg1, const point_2d &arg2);
point_2d operator +(const point_2d &arg1, const point_2d &arg2);
point_2d &operator -=(point_2d &arg1, const point_2d &arg2);
point_2d operator -(const point_2d &arg1, const point_2d &arg2);
point_2d &operator *=(point_2d &arg, double num);
point_2d operator *(const point_2d &arg, double num);
point_2d operator *(double num, const point_2d &arg);
point_2d &operator /=(point_2d &arg, double num);
point_2d operator /(const point_2d &arg, double num);
point_2d operator /(double num, const point_2d &arg);
point_2d normalizeVec(const point_2d &arg);
double vecHeading(const point_2d &arg);

double dotProduct(const point_2d &vec1, const point_2d &vec2);
double dist(const point_2d &vec1, const point_2d &vec2);
double norm(const point_2d &vec);
std::string toString(const point_2d &vec);
std::string toString(const sim::simulation_state &state);

template<typename T1, typename T2>
std::string toString(const std::map<T1, T2> &mapToPrint, const std::string &info)
{
    std::stringstream ss;
    ss << "Print: " << info << std::endl;
    for(const auto &kvp : mapToPrint)
    {
        ss << "Key: " << kvp.first<<", value: "<< kvp.second << "\n";
    }

    return ss.str();
}

#endif // UTIL_H
