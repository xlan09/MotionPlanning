#include "Predictor.h"
#include <cmath>
#include "simulator.h"

const double PI = 3.1415926;

sim::state_SE2 Predictor::vehicleDynamics(const sim::state_SE2 &vehiclePose, double linearVelocity, double angularVelocity, double dt)
{
    double x = vehiclePose.location.first;
    double y = vehiclePose.location.second;
    double theta = vehiclePose.theta;

    double tolerance = std::pow(10, -10);

    double oldTheta = theta;
    theta += angularVelocity * dt;

    // update model
    if (std::fabs(angularVelocity) < tolerance)
    {
        x += linearVelocity * std::cos(theta) * dt;
        y += linearVelocity * std::sin(theta) * dt;
    }
    else
    {
        double radius = linearVelocity / angularVelocity;
        x += radius * (std::sin(theta) - std::sin(oldTheta));
        y -= radius * (std::cos(theta) - std::cos(oldTheta));
    }

    if (theta > PI)
    {
        theta -= 2 * PI;
    }
    else if (theta < -1.0 * PI)
    {
        theta += 2 * PI;
    }

    return sim::state_SE2(x, y, theta);
}

