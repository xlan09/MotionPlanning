#ifndef SHAREDDATASERVICE_H
#define SHAREDDATASERVICE_H

#include <Core/SharedDataServiceDataStore.h>

typedef std::pair<double, double> point_2d;

class SharedDataService
{
public:
    SharedDataService();
    void pushToQueue(const point_2d &val);
    void popFromQueue();
    point_2d frontOfQueue() const;
    bool isQueueEmpty() const;
    std::size_t queueSize() const;
    std::queue<point_2d> getCurrQueue() const;

    sim::simulation_state getCurr() const;
    void setCurr(const sim::simulation_state &curr);

    sim::simulation_state getPrev() const;
    void setPrev(const sim::simulation_state &prev);

    sim::simulation_state getPrevOfPrev() const;
    void setPrevOfPrev(const sim::simulation_state &prevOfPrev);

private:
    std::shared_ptr<SharedDataServiceDataStore<point_2d>> m_datastore;
};

#endif // SHAREDDATASERVICE_H
