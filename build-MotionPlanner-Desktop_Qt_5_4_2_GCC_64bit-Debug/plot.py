#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

points = np.loadtxt('PointsOnSpline.txt', delimiter=',')
plt.figure()
plt.plot(points[:, 0], points[:, 1], 'ro-')

# plot trajectory by spline based planner
plt.figure()
trajectory_set = np.loadtxt('TrajectoryBySplineBasedPlanner.txt', delimiter=',')
trajectory_length = int(trajectory_set[0, -1])
num_trajectories = trajectory_set.shape[0] / trajectory_length
for i in range(num_trajectories):
    plt.plot(
    trajectory_set[i * trajectory_length : (i+1) * trajectory_length, 0],
    trajectory_set[i * trajectory_length : (i+1) * trajectory_length, 1], 'o-')

plt.show()
