#ifndef SPLINEPLANNER_H
#define SPLINEPLANNER_H

#include <vector>
#include <iostream>
#include <stdexcept>
#include <unordered_map>
#include <queue>
#include <thread>
#include <memory>

namespace spdlog
{
class logger;
}

class SharedDataService;

namespace sim
{
struct simulation_state;
struct state_SE2;
}

class Point;

typedef std::pair<double, double> point_2d;

typedef std::vector<point_2d> points_2d;

typedef int64_t guid;

/**
 * @brief The Planner class
 */
class SplinePlanner
{
public:
    SplinePlanner(double maxAngularVelocity, double neighborhoodRadius, double dt, std::shared_ptr<SharedDataService> sharedDataService, const points_2d &walls, unsigned int recedingHorizon=10);

    /**
     * @brief run
     * planning loop
     */
    void run();


    /**
     * @brief splineTrajectory
     * Use B-spline to create a spline of curvatures, control points are curvatures
     */
    std::vector<double> splineTrajectory(const sim::state_SE2 &startPose, double linearVelocity, const std::vector<Point> &controlPoints, unsigned int splineDegree, unsigned int timeSteps, double dt, std::vector<point_2d> &trajectory) const;

private:
    double m_maxAngularVelocity;
    double m_neighorhoodRadius;
    double m_dt; // control loop time step, unit is seconds
    std::shared_ptr<SharedDataService> m_sharedDataService;
    points_2d m_walls;
    unsigned int m_recedingHorizon;
    std::shared_ptr<spdlog::logger> m_logger;

    /**
     * @brief plan
     * planning
     */
    std::vector<point_2d> plan() const;

    /**
     * @brief findObstaclesInNeighborhood
     * Find obstacles inside a neighborhood region of the vehicle
     */
    std::vector<guid> findObstaclesInNeighborhood(std::unordered_map<guid, sim::state_SE2> &predictedObstaclePoseMap, const point_2d &vehiclePos, double neighborhoodRadius) const;

    /**
     * @brief evaluateTrajectoryToObstacle
     * Evaluate trajectory to one obstacle and walls
     */
    double evaluateTrajectoryToObstacle(const sim::state_SE2 &obsPose, const point_2d &obsCommands, const points_2d &walls, const std::vector<point_2d> &trajectory, double dt, double safeDistToObs, double safeDistToWall, unsigned int &feasibleTrajEndPointIndex) const;

    /**
     * @brief evaluateTrajectory
     * Evaluate the trajectory to all the obstacles inside its neighborhood
     */
    double evaluateTrajectory(const std::vector<guid> &nearIds, const std::unordered_map<guid, sim::state_SE2> &predictedObstaclePoseMap, const std::unordered_map<guid, point_2d> &obsIdToCommandsMap, const points_2d &walls, const std::vector<point_2d> &trajectory, double dt, double safeDistToObs, double safeDistToWall, unsigned int &feasibleTrajEndPointIndex) const;
};

/**
 * @brief The Point class
 */
class Point
{
public:
    Point();
    Point(double x);
    Point(double x, double y);

    double &operator[](unsigned int i);
    const double &operator [](unsigned int i) const;

    Point &operator +=(const Point &rhs);
    Point &operator +=(double rhs);
    Point &operator -=(const Point &rhs);
    Point &operator -=(double rhs);
    Point &operator *=(const Point &rhs);
    Point &operator *=(double rhs);

    friend std::ostream &operator <<(std::ostream &os, const Point &point);
    friend std::stringstream &operator <<(std::stringstream &ss, const Point &point);

private:
    double m_x;
    double m_y;
};

Point operator +(const Point &lhs, const Point &rhs);
Point operator -(const Point &lhs, const Point &rhs);
Point operator *(const Point &lhs, double rhs);
Point operator *(double lhs, const Point &rhs);

/**
 * @brief The Spline class
 */
class Spline
{
public:
    Spline(const std::vector<Point> &controlPoints, unsigned int degree, double timeSpan=1);
    Point value(double t) const;
    std::vector<double> knots() const;

private:
    std::vector<Point> m_controlPoints;
    unsigned int m_degree;
    std::vector<double> m_knots;

    /**
     * @brief b_i_0: basis function
     * B_i_0(t) = 1 if t_i <= t <= t_{i+1} else 0
     */
    double b_i_0(unsigned int i, double t) const;
    double b_i_d(unsigned int i, unsigned int degree, double t) const;
};

#endif // SPLINEPLANNER_H
