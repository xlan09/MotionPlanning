#ifndef SHAREDDATASERVICEDATASTORE_H
#define SHAREDDATASERVICEDATASTORE_H

#include <mutex>
#include <queue>
#include "simulator.h"

template<typename T>
class SharedDataServiceDataStore
{
public:
    SharedDataServiceDataStore();
    SharedDataServiceDataStore(const SharedDataServiceDataStore &copy) = delete;
    SharedDataServiceDataStore &operator =(const SharedDataServiceDataStore &val) = delete;

    void pushToQueue(const T &val);
    void popFromQueue();
    T frontOfQueue() const;
    bool isQueueEmpty() const;
    std::size_t queueSize() const;
    std::queue<T> getCurrQueue() const;

    sim::simulation_state getCurr() const;
    void setCurr(const sim::simulation_state &curr);

    sim::simulation_state getPrev() const;
    void setPrev(const sim::simulation_state &prev);

    sim::simulation_state getPrevOfPrev() const;
    void setPrevOfPrev(const sim::simulation_state &prevOfPrev);

private:
    /**
     * @brief m_maxQueueSize
     * If queue too large, we do not want to push element into it
     */
    mutable std::mutex m_mu;
    std::queue<T> m_actionQueue;
    sim::simulation_state m_curr;
    sim::simulation_state m_prev;
    sim::simulation_state m_prevOfPrev;
};

#endif // SHAREDDATASERVICEDATASTORE_H
