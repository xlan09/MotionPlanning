#ifndef SCOPEDTHREAD_H
#define SCOPEDTHREAD_H
#include <thread>

class ScopedThread
{
public:
    ScopedThread(std::thread t);
    ~ScopedThread();

    ScopedThread(const ScopedThread &copy) = delete;
    ScopedThread &operator =(const ScopedThread &val) = delete;

private:
    std::thread m_t;
};

#endif // SCOPEDTHREAD_H
