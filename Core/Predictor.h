#ifndef PREDICTOR_H
#define PREDICTOR_H

#include <utility>

namespace sim {
   struct state_SE2;
}

typedef std::pair<double, double> point_2d;

class Predictor
{
public:

    /**
     * @brief vehicleDynamics
     */
    static sim::state_SE2 vehicleDynamics(const sim::state_SE2 &vehiclePose, double linearVelocity, double angularVelocity, double dt);
};

#endif // PREDICTOR_H
