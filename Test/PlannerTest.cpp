#include "catch.hpp"
#include "Core/SplinePlanner.h"
#include "Core/Util.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include "simulator.h"
#include <stdexcept>
#include "Core/Estimator.h"
#include "Core/SharedDataService.h"

const double PI = 3.1415926;

TEST_CASE("PlannerUtil Test")
{
    double expected[] = {0, 0.2, 0.4, 0.6, 0.8, 1, 1.2};
    std::vector<double> res = Util::linspace(0, 1.2, 7);
    for(std::size_t i = 0; i < res.size(); ++i)
    {
        REQUIRE(std::fabs(res[i] - expected[i]) < 0.000001);
    }

    res = Util::linspace(0, 0.3, 2);
    REQUIRE(std::fabs(res[0] - 0) < 0.000001);
    REQUIRE(std::fabs(res[1] - 0.3) < 0.000001);
}

TEST_CASE("PlannerSpline Test")
{
    std::vector<Point> controlPoints;
    controlPoints.push_back(Point(0, 0));
    controlPoints.push_back(Point(1, 1));
    controlPoints.push_back(Point(2, 0));
    controlPoints.push_back(Point(3, 1));
    controlPoints.push_back(Point(4, 0));

    Spline spline(controlPoints, 2);
    std::vector<double> times = Util::linspace(0, 1, 50);
    std::vector<Point> pointsOnSpline;
    for(std::size_t i = 0; i < times.size(); ++i)
    {
        pointsOnSpline.push_back(spline.value(times[i]));
    }

    std::ofstream file("PointsOnSpline.txt");
    if(file.is_open())
    {
        for(std::size_t i = 0; i < pointsOnSpline.size(); ++i)
        {
            file << pointsOnSpline[i][0] << ", " << pointsOnSpline[i][1] << std::endl;
        }
        file.close();
    }
    else
    {
        std::cout << "Can not open file to store points on spline" << std::endl;
    }

    // print knots
    std::vector<double> knots = spline.knots();
    std::cout << "Knots are as follows." << std::endl;
    for(std::size_t i = 0; i < knots.size(); ++i)
    {
        std::cout << knots[i] << ", ";
    }
    std::cout << std::endl;
}

TEST_CASE("Util Itertools.product Test")
{
    double maxCurvature = 2;
    std::vector<double> controlPointsSet = Util::linspace(-maxCurvature, maxCurvature, 5);
    std::vector<std::vector<double> > res;
    Util::iterProduct(controlPointsSet, 3, res);
    std::ofstream file("Permutation.txt");
    if(file.is_open())
    {
        for(std::size_t i = 0; i < res.size(); ++i)
        {
            for(std::size_t j = 0; j < res[0].size(); ++j)
            {
                file << res[i][j] << ", ";
            }
            file << std::endl;
        }
        file.close();
    }
    else
    {
        std::cout << "Can not open file to print permutation!" << std::endl;
    }
}

TEST_CASE("PlannerSplineTrajectory Test")
{
    double dt = 0.1;
    double maxLinearVelocity = 0.8, maxAngularVelocity = PI / 3, neighborhood = 5;
    unsigned int splineDegree = 2, recedingHorizon = 5;
    std::shared_ptr<SharedDataService> sharedDataPtr = std::shared_ptr<SharedDataService>(new SharedDataService());
    std::vector<std::pair<double, double> > walls;
    SplinePlanner planner(maxAngularVelocity, neighborhood, dt, sharedDataPtr, walls, recedingHorizon);
    double maxCurvature = 3;
    std::vector<double> controlPointsSet = Util::linspace(-maxCurvature, maxCurvature, 5);

    // generate control points
    unsigned int numControlPoints = splineDegree + 1;
    std::vector<std::vector<double> > controlPoints;
    Util::iterProduct(controlPointsSet, numControlPoints, controlPoints);
    sim::state_SE2 startPose(1, 1, PI / 4.0);

    // open file to store trajectory
    std::ofstream file("TrajectoryBySplineBasedPlanner.txt");

    for(std::size_t i = 0; i < controlPoints.size(); ++i)
    {
        std::vector<Point> points;
        for(std::size_t j = 0; j < controlPoints[0].size(); ++j)
        {
            points.push_back(Point(controlPoints[i][j]));
        }

        std::vector<sim::point_2d> trajectory;
        std::vector<double> steerAngles = planner.splineTrajectory(startPose, maxLinearVelocity, points, splineDegree, recedingHorizon, dt, trajectory);

        for(std::size_t j = 0; j < trajectory.size(); ++j)
        {
            if(file.is_open())
            {
                // print trajectory length to prepare for plotting
                file << trajectory[j].first << ", " << trajectory[j].second << ", " << trajectory.size() << std::endl;
            }
            else
            {
                throw std::logic_error("Can not open file to store trajectory by planner");
            }
        }
    }
    file.close();
}

TEST_CASE("Estimator Test")
{
    double tolerance = 0.0001;

    // move along a circular arc with radius 0.5
    sim::state_SE2 curr(0.5 * std::cos(PI/4), 0.5 * std::sin(PI/4), 3 * PI / 4.0);
    sim::state_SE2 prev(0, -0.5, 0);
    sim::point_2d commands = Estimator::estimate(curr, prev, 1);
    REQUIRE(std::fabs(commands.first- 3 * PI / 4.0 * 0.5) < tolerance);
    REQUIRE(std::fabs(commands.second - 3 * PI / 4.0) < tolerance);

    // only rotation, counter clockwise
    curr = sim::state_SE2(0, 0, PI/4);
    prev = sim::state_SE2(0, 0, -PI/2);
    commands = Estimator::estimate(curr, prev, 1);
    REQUIRE(std::fabs(commands.first - 0) < tolerance);
    REQUIRE(std::fabs(commands.second - 3 * PI / 4.0) < tolerance);

    // rotation, clockwise
    curr = sim::state_SE2(0, 0, 3 * PI/4);
    prev = sim::state_SE2(0, 0, - 3 * PI/4);
    commands = Estimator::estimate(curr, prev, 1);
    REQUIRE(std::fabs(commands.first - 0) < tolerance);
    REQUIRE(std::fabs(commands.second - (-PI / 2.0)) < tolerance);
}
