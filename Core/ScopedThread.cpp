#include "ScopedThread.h"

ScopedThread::ScopedThread(std::thread t) : m_t(std::move(t))
{
    if(!m_t.joinable())
    {
        throw std::logic_error("No thread");
    }
}

ScopedThread::~ScopedThread()
{
    m_t.join();
}
